import java.util.Scanner;

public class Application {
	public static void main(String[] args){
		
		Student jenny = new Student();
		jenny.setHeight(162.3);
		jenny.setAge(17);
		System.out.println(jenny.getFavColor());
		jenny.setFavColor("Green");
		System.out.println(jenny.getFavColor());
		
		Student haris = new Student();
		haris.setHeight(194.7);
		haris.setAge(19);
		haris.setFavColor("Black");
		
		Student[] section4 = new Student[3];
		section4[0] = jenny;
		section4[1] = haris;
		section4[2] = new Student();
		section4[2].setHeight(178.5);
		section4[2].setAge(21);
		section4[2].setFavColor("Blue");
		
		System.out.println(jenny.getHeight());
		System.out.println(jenny.getAge());
		System.out.println(jenny.getFavColor());
		System.out.println();
		System.out.println(haris.getHeight());
		System.out.println(haris.getAge());
		System.out.println(haris.getFavColor());
		System.out.println();

		jenny.sayHeight(jenny.getHeight());
		haris.sayHeight(haris.getHeight());
		System.out.println();

		System.out.println(section4[2].getHeight());
		System.out.println(section4[2].getAge());
		System.out.println(section4[2].getFavColor());

		System.out.println(section4[0].amountLearnt);
		
		Scanner scan = new Scanner(System.in);
		
		int amountStudied = scan.nextInt();
		section4[2].learn(amountStudied);
		section4[2].learn(amountStudied);
		
		System.out.println(section4[0].amountLearnt);
		System.out.println(section4[1].amountLearnt);
		System.out.println(section4[2].amountLearnt);
		
		System.out.println(section4[0].amountLearnt);
		section4[0].learn(10);
		section4[0].learn(-5);
		System.out.println(section4[0].amountLearnt);
	}
}
	
	