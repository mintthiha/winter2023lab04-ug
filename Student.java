public class Student {
	private double height;
	private int age;
	private String favColor;
	int amountLearnt;
	boolean isValid;
	
	public void sayHeight(double height){
		System.out.println("my height is " + height);
	}
	
	public void sayAge(int age){
		System.out.println("My age is " + age);
	}
	
	public void learn(int amountStudied){
		
		if( amountStudied > 0)
		{
			amountLearnt += amountStudied;
		}
	}
	
	private boolean isNumValid(int amountStudied){
		if (amountStudied < 0)
		{
			boolean isValid = false;
		}
		else{
			boolean isValid = true;
		}
		return isValid;
	}
	
	public double getHeight(){
		return this.height;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public String getFavColor(){
		return this.favColor;
	}
	
	public void setFavColor(String newFavColor){
		this.favColor = newFavColor;
	}
	
	public void setHeight (double newHeight){
		this.height = newHeight;
	}
	
	public void setAge (int newAge){
		this.age = newAge;
	}
}
	